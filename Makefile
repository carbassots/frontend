DOCKER_TAG := latest
REGISTRY_HOST := registry.gitlab.com
USERNAME := carbassots
NAME := frontend

all: build push

build:
	docker build -t $(REGISTRY_HOST)/$(USERNAME)/$(NAME):$(DOCKER_TAG) .

push:
	docker push $(REGISTRY_HOST)/$(USERNAME)/$(NAME):$(DOCKER_TAG)

run:
	docker run --rm --name mobilehack-front -p 80:80 $(REGISTRY_HOST)/$(USERNAME)/$(NAME):$(DOCKER_TAG)
