import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Access from '../views/access/Access.vue'
import Login from '../views/access/Login.vue'
import SignUp from '../views/access/SignUp.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
  component: Access,
   path: '/login',
   children: [
     {
       path: '/signup',
       name: 'signup',
       component: SignUp
     },
     {
       path: '',
       name: 'login',
       component: Login
     }
   ]
  },
  {
    name: 'Admin',
    path: '/admin',
    component: () => import ('@/views/Admin.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
