import Vue from 'vue'
import Vuex from 'vuex'
import { login, getUserData, signup, getUsers, getShops, getPayments, getTotalData, getTransactions } from '@/api/cargols.api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    username: '',
    login: false,
    email: '',
    coins: 0,
    usermode: '',
    token: '',
    users: [],
    shops: [],
    payments: [],
    totalData: {},
    transactions: []
  },
  mutations: {
    auth_login (state, userdata) {
      console.log(userdata)
      state.login = true
      state.token = userdata.access_token
    },
    set_user_data (state, userdata) {
      state.username = userdata.username
      state.email = userdata.email
      state.coins = userdata.coins
    },
    set_user_list(state, data) {
      state.users = data
    },
    set_shops_list(state, data) {
      state.shops = data
    },
    set_payments_list(state, data) {
      state.payments = data
    },
    set_total_data(state, data) {
      state.totalData = data
    },
    set_transations(state, data) {
      state.transactions = data
    }
  },
  actions: {
    getUsers: async ({commit}) => {
      const response = await getUsers()
      commit('set_user_list', response)
    },
    login: async({ commit }, data) => {
      try {
        const response = await login(data)
        commit('auth_login', response)
        const userdata = await getUserData()
        commit('set_user_data', userdata)
        return true
      } catch (e) {
        console.log(e)
      }
    },
    getTotalData: async({ commit }) => {
      try {
        const response = await getTotalData()
        console.log('dsad')
        commit('set_total_data', response)
      } catch (e) {
        console.log(e)
      }
    },
    getTransactions: async({ commit }) => {
      try {
        const response = await getTransactions()
        commit('set_transactions', response)
      } catch (e) {
        console.log(e)
      }
    },
    getShops: async({ commit }) => {
      try {
        const response = await getShops()
        commit('set_shops_list', response)
      } catch (e) {
        console.log(e)
      }
    },
    getPayments: async({ commit }) => {
      try {
        const response = await getPayments()
        commit('set_payments_list', response)
      } catch (e) {
        console.log(e)
      }
    },
    signup: async(_, data) => {
      try {
        await signup(data)
        return true
      } catch (e) {
        console.log(e)
      }

    }
  },
  modules: {
  },
  getters: {
    username: state => state.username,
    isLogged: state => state.login,
    getToken: state => state.token,
    getUsers: state => state.users,
    getShops: state => state.shops,
    getPayments: state => state.payments,
    getTotalData: state => state.totalData,
    getTransactions: state => state.transactions
  }
})
