import axios from '@/plugins/axios.js';
import store from '@/store'


const signup = async (data) => {
  const response = await axios.post('/users', data, {
    "headers" : {
      "Content-Type": "application/json"
    }
  })
  if ( response.status === 201 )
    return response.data
  else
    throw ({status: response.status})
}

const login = async (data) => {

  const response = await axios.post('/users/login', data, {
    "headers" : {
      "Content-Type": "application/json"
    }
  })
  if ( response.status === 200 )
    return response.data
  else
    throw ({status: response.status})

}

const getUserData = async () => {
  const response = await axios.get('/users/me', {
    "headers" : {
      "Authorization" : `Bearer ${store.getters.getToken}`
    }
  })
  if ( response.status === 200 )
    return response.data
  else
    throw ({status: response.status})

}

const userList = async () => {

}


const getPayments = async () => {

  const response = await axios.get('/payment/', {
    "headers" : {
      "Authorization" : `Bearer ${store.getters.getToken}`
    }
  })

  if ( response.status === 200 )
    return response.data
  else
    throw ({status: response.status})

}

const getUsers = async () => {

  const response = await axios.get('/users/', {
    "headers" : {
      "Authorization" : `Bearer ${store.getters.getToken}`
    }
  })

  if ( response.status === 200 )
    return response.data
  else
    throw ({status: response.status})

}

const getShops = async () => {
  const response = await axios.get('/stores/', {
    "headers" : {
      "Authorization" : `Bearer ${store.getters.getToken}`
    }
  })
  if ( response.status === 200 )
    return response.data
  else
    throw ({status: response.status})

}

const getTotalData = async () => {
  const response = await axios.get('/statistics/overview', {
    "headers" : {
      "Authorization" : `Bearer ${store.getters.getToken}`
    }
  })
  if ( response.status === 200 )
    return response.data
  else
    throw ({status: response.status})

}
const getTransactions = async () => {
  const response = await axios.get('/statistics/daily/', {
    "headers" : {
      "Authorization" : `Bearer ${store.getters.getToken}`
    }
  })
  if ( response.status === 200 )
    return response.data
  else
    throw ({status: response.status})

}

export {
  signup,
  login,
  getUserData,
  userList,
  getUsers,
  getShops,
  getPayments,
  getTotalData,
  getTransactions
}
